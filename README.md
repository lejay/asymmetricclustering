# Asymmetric clustering

This is a port in R of the computation of the asymmetric spectral clustering method proposed by S.E. Atev in his thesis (University of Minnesota, 2011).
In the thesis, a Matlab code is proposed. 


## Details

Details are given in 
- S. E. Atev, *Using Asymmetry in the Spectral Clustering of Trajectories*, University of Minnesota, 2011.
- A. Lejay, *[Asymmetric Spectral Clustering](https://hal.inria.fr/hal-02372570)*, Inria Technical report, 2019.


## Installation and package creation

To create the documentation 
```
    devtools::document()  
```

To create the package
```
    devtools::build()
```

The package is built on the parent directory.

To install from AsymmetricClustering_XXX.tar.gz : to into the suitable directory and set
```
install.packages("AsymmetricClustering", repos = NULL,type='source')
```
