#' AsymmetricClustering: A package for clustering from asymmetric matrices
#'
#' The AsymmetricClustering package provides a function to create clusters
#' from an asymmetric matrix based on the eigenvectors and eigenvalues
#' for a related affinity matrix. This is an R port from the Matlab code
#' and the technique developed in S.E. Atev thesis "Using Asymmetry in the Spectral Clustering of Trajectories".
#'
#' @docType package
#' @name AsymmetricClustering
#'
library(complexplus) # for matexp (exponential of matrices)
library(settings) # for dealing with global configuration variables


# Environment for the global configuration variables 


#' Softmax function 
#'
#' `softmax` is a softmax function to normalize vector.	This implementations 
#' tries to avoid to production of `NaN`. 
#'
#' The softmax function returns the vector `exp(x)/sum (exp(x))`. 
#' As adding a constant to `x` do not change the value, we add the maximum
#' of the elements of `x`.
#' 
#' @param x An input vector
#' @param scale The scale
#' 
#' @return An output, normalized vector
softmax <- function(x,scale) {
    y<-(x-max(x))/scale # scale the input, get non-positive values
    return(exp(y)/sum(exp(y)))
}

# Create new options, for the global settings
# - `precision` Threshold to decide if a quantity is equal to 0 
# - `optimize_over_unitary_tolerance` Threshold for the unitary optimization step
# - `polynomial_approximation_order` Order of the polynomial approximation in the line search
# - `optimize_over_unitary_max_iter` Maximum iteration in the unitary optimization step
# - `clustering_asymmetric_matrix_max_iters` Maximum iteration in the clustering algorithm
# - `clustering_asymmetric_matrix_min_improvement` Threshold for the relative improvement for the clustering algorithm
# - `update_weights_threshold_sigmoid` Threshold for the values in a column of the softmax function, in which case it does not use the exponential
# - `softmax` Choice of the softmax function
AsymmetricClusteringOptions<-settings::options_manager(
precision=sqrt(.Machine$double.eps),
optimize_over_unitary_tolerance=1e-7, 
polynomial_approximation_order=3,
optimize_over_unitary_max_iters=100,
clustering_asymmetric_matrix_max_iters=50,
clustering_asymmetric_matrix_min_improvement=1e-3,
update_weights_threshold_sigmoid=1e-6, 
softmax=softmax
) 


#' Check the dimension of a matrix
#'
#' `is.dim` check if a matrix has the given number of rows and columns.
# 
#' @param m matrix
#' @param k number of rows
#' @param n number of columns
#'
#' @return boolean, is the dimension are all right.
is.dim<-function(m,k,n)
{
    return(nrow(m)==k & ncol(m)==n)
}

#' Hermitian transpose
#'
#' `HT` returns the Hermitian transpose of a matrix, i.e., transpose of the conjugute matrix.
#'
#' @param m matrix
#'
#' @return matrix, the conjugate transpose of m, 
#'
#' @export
HT<-function(m)
{
    return(Conj(t(m)))
}

#' Check a a matrix is unitary
#' 
#' Check if \eqn{m m^H=I}, up to some precision.
#' 
#' @param m matrix
#'
#' @return TRUE if the matrix is unitary, FALSE otherwise
is.unitary<-function(m)
{
    stopifnot(is.dim(m,nrow(m),nrow(m))) # Check if the matrix is square
    return(is.zero(diag(nrow(m))- HT(m) %*% m));
}

#' Check a a matrix is hermitian
#' 
#' Check if \eqn{m=m^H}, up to some precision.
#' 
#' @param m matrix
#'
#' @return TRUE if the matrix is unitary, FALSE otherwise
is.hermitian<-function(m)
{
    stopifnot(is.dim(m,nrow(m),nrow(m))) # Check if the matrix is square
    return(is.zero(m- HT(m)));
}



#' Check if a float, vector, matrix is equal to 0, up to a tolerance
#'
#' Generic method for checking if an object is zero, up to a precision.
#'
#' @param x a vector
#' @param precision tolerance
#'
#' @return a logical value
is.zero<-function(x,precision) UseMethod("is.zero")

#' Check if a float/vector is equal to 0
#' 
#' @describeIn is.zero Check if a vector vanishes.
#'
#' @return TRUE if `x` is zero (up to a given precision), FALSE otherwise
is.zero.numeric<-function(x,precision=AsymmetricClusteringOptions("precision")) 
{ 
    return(all(abs(x)<precision))
}

#' Check if the coefficients of a matrix are equal to 0
#'
#' @describeIn is.zero Check if a matrix vanishes.
is.zero.matrix<-function(x,precision=AsymmetricClusteringOptions("precision")) 
{ 
    return(is.zero(norm(abs(x)),precision))
}


#' Check if a complex vector is real
#'
#' `is.real` checks if an object has no imaginary part.
#'
#' @param x object to check
#' @param precision tolerance
#'
#' @return TRUE if `x` is real and FALSE otherwise.
#
#' @return a logical value
is.real<-function(z,precision) UseMethod("is.real")

#' Check if a complex vector is real
#'
#' @describeIn is.real Check if a complex vector is real, i.e., has no imaginary part.
is.real.complex <- function(x,precision=AsymmetricClusteringOptions("precision")) 
{
    return(sapply(x,function(y){is.zero(Im(y),precision)}))
}

#' Check if a vector is empty.
#' 
#' @param x vector
#'
#' @return a logical value
is.empty <- function(x)
{
    return(length(x)==0L)
}

#' Compute a Hermitian matrix
#' 
#' `to_hermitian_matrix` create a complex matrix 
#' 
#' @param A real matrix
#'
#' @return A matrix whose real part is the symmetric part and the imaginary part is the skew-symmetric part.
#'
#' @export
to_hermitian_matrix<- function(A)
{
    return(0.5* (( A + t(A))+1i*(A - t(A))))
}

#' Spectral embedding
#'
#' 'spectral_embedding' changes the coordinates of a Hermitian matrix to create
#' a new matrix on which the cluster analysis will be performed
#'
#' @param A Hermitian matrix of size n x n
#' @param C Number of clusters
#'
#' @return A matrix of size k x n 
#'
#' @export
spectral_embedding <- function(A,C)
{
    stopifnot(is.hermitian(A)) # Check if A is hermitian
    stopifnot(C==as.integer(C)) # Check if C is an integer (do not use is.integer here)
    eigen_elements<- eigen(A,symmetric=TRUE) # Compute the eigenelements
    # The eigenvalues are ordered in decreasing order by default (see the doc)
    # The eigenvectors are stored in the columns of eigen_elements$vectors
    L<- as.complex(eigen_elements$values[1:C]) # Keep only the C largest eigenvalues
    V<- eigen_elements$vectors[,1:C] # the eigenvectors, a matrix, size n x C
    Y<- diag(1/sqrt(L)) %*% HT(V) # complex matrix of size C x n
    return(Y) # The spectral embedding of the Hermitian matrix A
}

#' Update the scale parameter
#'
#' `update_scale_parameter` updates the scale parameters, uses to concentrate the weights.
#'
#' @param sigma, the value of scale parameter (the cost divide by the dimension of Y)
#' @param iter the number of iterations
#'
#' @return A new scale parameter 
#' 
update_scale_parameter <- function(sigma,iter)
{
    return(sigma/iter)
}

#' Clustering algorithm 
#'
#' `clustering asymmetrix matrix` computes C clusters for an antisymmetric matrices. 
#'
#' @param A Matrix
#' @param C Number of clusters
#' @param embedding A function to perform the spectral embedding
#' @param update_scale A function to update the scale parameter
#' 
#' @return a list with idx, the index for each cluster and weight, the maximal weight seen as a confidence value, the distance to the closest direction and the unitary matrix
#'
#' @export
clustering_asymmetric_matrix <- function(A, C, to_hermitian=to_hermitian_matrix, embedding=spectral_embedding, update_scale=update_scale_parameter)
{
    stopifnot(is.matrix(A)) # Check if A is a matrix
    stopifnot(nrow(A)==ncol(A)) # Check if A is not a square matrix
    stopifnot(C==as.integer(C)) # Check if C is an integer (do not use is.integer here)
    stopifnot(C<=nrow(A)) # We should have C ≤ n

    costs<-c() # to store the cost function
    # maximal number of iterations
    maxOuterIters <- AsymmetricClusteringOptions("clustering_asymmetric_matrix_max_iters") 
    # threshold for stopping the algorithm
    minImprovement  <-  AsymmetricClusteringOptions("clustering_asymmetric_matrix_min_improvement") 
    n <- nrow(A) # Number of rows of n
    K<- to_hermitian_matrix(A) # Complex matrix of size n x n
    Y<- embedding(K,C) # Compute the number of clusters
    U<-  diag(C) # diagonal identity matrix of size CxC
    psigma <- max(columnwiseOp(abs(Y),min)) # sigma0, maximum of the minimum of the column, a scalar
    W<-update_weights(U=U, Y=Y, sigma=psigma) # W is a list is a weight matrix and a scalar
    for (iter in 1:maxOuterIters){
	U<-optimize_over_unitary(U=U,Y=Y,W=W$weights)  # update the unitary matrix
	W<-update_weights(U=U, Y=Y, sigma=psigma ) # update the weights
	costs<-cbind(costs,W$cost) # store the cost function
	sigma<- update_scale(W$sigma,iter) # update the temperature
	# Break of the new iteration does not improve so much
	if ((sigma < psigma) & (sigma > (1 - minImprovement )* psigma) & (iter > 1))
	    { break }
	psigma = sigma  # update psigma
	}
    # Check, consistency with Matlab 
    idx <- columnwiseOp(W$weights,which.max) # compute the index at which the maximum is reached on each row
    maxi <- columnwiseOp(W$weights, max) # compute the maximum at which the maximum is reached on each row
    clusters<-list(index=idx,weight=maxi,costs=as.vector(costs))
    clusters$distance<- apply(sqrt(compute_squared_distance(U,Y)),2,min) # compute the distance with the unitary matrix
    clusters$unitary<-U # returns the unitary matrix of directions
    return(clusters)
}

#' Apply a function of each column, to avoid confusion with matlab
#'
#' `columnwiseOp` apply a function on each column. This function is introduced for more readability.
#'
#' @param m a matrix
#'
#' @return a vector, whose length is the number of columns of m
columnwiseOp<-function(m,func)
{
    return(apply(m,2,func))
}

#' Compute the distance 
#'
#' `compute_squared_distance` compute the distances between the columns of Y and a unitary matrix
#'
#' @param U a matrix of size C x C, should be unitary
#' @param Y a matrix of size C x n
#' 
#' @return A matrix of size C x n with the distance between the elements of Y and the ones of U, 
#' more precisely, D[i,j] is the distance between U[,i] and Y[,j]
compute_squared_distance<- function(U,Y)
{
    k<- nrow(Y) # should be equal to C, the number of clusters
    n<- ncol(Y) # should be equal to n, the number of rows/columns of the initial matrix
    D<- matrix(0,k,n) # initialize the matrix for the square of the distance
    for (i in 1:k)
    {
	P<- diag(k)- U[,i] %*% HT(U[,i]) # matrix of size k x k
	for(j in 1:n)
	    D[i,j] <- HT(Y[,j]) %*% P %*% Y[,j] # square of the distance
    }
    D<-Re(D) 
    stopifnot(all(D>=0)) # The distance shall be non-negative
    return(D)
}


#' Helper function to compute the cost
#' 
#' @param U a matrix of size C x C, should be unitary
#' @param Y a matrix of size C x n
#' @param W matrix C x n (@warning the convention is different from the one of Atev's thesis)
#'
#' @return a scalar
compute_cost<- function(U,Y,W)
{
    return(sum(sum(W*compute_squared_distance(U,Y))))
}

#' Update the weights
#'
#' `update_weights` update the weight in each axis.
#'
#' @param U a matrix of size C x C, should be unitary
#' @param Y a matrix of size C x n
#' @param sigma the scale parameter
#'
#' @return a list with two elements
#'   - matrix matrix the new weights, of size C x n
#'   - float scalar with the updated value of sigma
update_weights<- function(U ,Y , sigma)
{
    k<- nrow(Y) # should be equal to C, the number of clusters
    n<- ncol(Y) # should be equal to n, the number of rows/columns of the initial matrix
    D<-compute_squared_distance(U,Y) # compute the squared distances
    W<-apply(-D,2,function(x){AsymmetricClusteringOptions("softmax")(x,sigma)}) 
    # Apply the softmax functions to the distances
    # W is a matrix of size C x n 
    # The sum of the columns of W shall be equal to 1 
    cost<-sum(sum(D*W)) # the cost function
    sigma <- cost/n # scalar, to update the value of sigma
    return(list(weights=W, sigma=sigma, cost=cost)) # return the result as a list
}



#' Trace of the real part of a matrix.
#' 
#' `trace_Re` returns the trace of the real part of a matrix.
#'
#' @param m matrix
#' @return a scalar, trace of the real part
trace_Re<-function(m)
{
    return(sum(diag(Re(m))))
}

#' Hermitian inner product
#' 
#' `hermitian_inner_prod` compute the inner product for hermitian matrices
#' 
#' @param U a matrix
#' @param V a matrix
hermitian_inner_prod<-function(U,V)
{
    return(0.5*trace_Re(HT(U) %*% V))
}


#' Optimize over unitary matrices
#'
#' `optimize_over_unitary` optimize the value function on the manifold of unitary matrices.
#' 
#' @param U matrix CxC
#' @param Y matrix Cxn
#' @param W matrix Cxn 
#'
#' @return a unitary matrix
optimize_over_unitary <- function(U, Y, W){
  # cost_start<- compute_cost(U,Y,W) # CHECK
  n <- nrow(U) # extract the number of rows
  maxiter <- min(c(AsymmetricClusteringOptions("optimize_over_unitary_max_iters"), ceiling(2*log(n)*(n**2)))) # maximum iteration
  dU<- gradient(U=U,Y=Y,W=W) # compute the gradient
  G<- dU %*% HT(U)-U %*% HT(dU) # # the steepest direction
  H<- G # the descent direction, a matrix of size C x C
  gg <- hermitian_inner_prod(G,G) # a scalar, should converge to 0
  iters <- 0;
  while (iters < maxiter & gg > AsymmetricClusteringOptions("optimize_over_unitary_tolerance"))
  {
    iters <- iters + 1; # update the number of ierations
    mu<- line_search (U=U,H=H,Y=Y,W=W) # minimize the cost function over the geodesics
    if (is.na(mu) & (is.zero(H-G)))
	{ return (U) }
    U<- complexplus::matexp(-mu*H) %*% U  # compute the next points
    stopifnot(is.unitary(U)) # check if the matrix is unitary
    # Update the direction search
    dU<- gradient(U=U,Y=Y,W=W) # the gradient 
    Gn <- dU %*% HT(U) - U %*% HT(dU) # the new steepest direction
    gk <- hermitian_inner_prod(Gn-G,Gn) / hermitian_inner_prod(G,G) # a scalar, the approxiate Polak-Ribière formula
    G<-Gn # update G
    gg <- hermitian_inner_prod(G,G) # the inner product in the Riemannian manifold of unitary matrices
    H <- G+ gk*H
    if (((iters %% n**2)== 0) | (hermitian_inner_prod(H,G)<0))  
      { H <- G }
  }
  return(U)
}


#' Compute the gradient of the value function 
#'
#' `gradient` computes the gradient of the value function in the direction of the unitary matrices.
#'
#' @param U matrix C x C 
#' @param Y matrix C x n
#' @param W matrix C x n 
#' 
#' Warning The convention for W is different from the one of Atev's thesis.
#'
#' @return a matrix
gradient <-function(U,Y,W){
    k<- nrow(Y)
    n<- ncol(Y)
    dU <- matrix(0,nrow(U),ncol(U))
    ConjYTU=HT(Y) %*% U
    for(row in 1:k)
	for(col in 1:k)
	    { dU[row,col]<- sum(sapply(1:n,function(i) {W[col,i]*ConjYTU[i,col]*Y[row,i]})) }
  return(-dU) # beware of the negative sign
}

#' Search the minimal of a function along a geodesic line 
#' 
#' Search the minimal of a function along a geodesic line by using a polynomial approximation of the cost function.
#'
#' @param U matrix C x C 
#' @param H matrix C x C 
#' @param Y matrix C x n
#' @param W matrix C x n (@warning the convention is different from the one of Atev's thesis)
#'
#' @return NA (if failure) or a scalar
line_search <- function(U,H,Y,W){
  q=2 # this is hard coded, as the cost function is quadratic in U
  P=AsymmetricClusteringOptions("polynomial_approximation_order") # order of the polynomial approximation

  k<- nrow(Y) # number of clusters
  n<- ncol(Y) # size of the data
  
  wmax <- max(abs(eigen(H)$values)) # eigenvalue with maximal norm
  Tmu <- 2*pi/(q*wmax) # largest frequency
  R1 <- complexplus::matexp(-(Tmu/P)*H) # matrix of size CxC
  gmu <- rep(0,P+1)  # a vector of zeros
  R <- diag(nrow(R1)) # matrix of size CxC
  # compute the gradient at several points
  for (i in 1:(P+1))
  {
    dU <- gradient(U=R%*%U,Y=Y,W=W) # compute the gradient 
    gmu[i] <- 2*Re(sum(diag(dU %*% HT(U) %*% HT(R) %*% HT(H)))) # compute the Riemannian gradient
    R <- R1 %*% R # update the position of the geodesic
  }
  J <- gmu[2:length(gmu)]-gmu[1] 
  M <- matrix(0, P, P)
  for(i in 1:P)
      { M[i,] <- (i*Tmu/P)**(1:P) } # fill the rows of the matrices M with the powers of Tmu/P
  a<- qr.solve(M,J) # 
  # a is a vector for the coefficient of a polynomial which approximate the function
  a<-c(gmu[1],a) # add the initial value of the Riemaniann gradient
  z<-polyroot(a) # Find the root of the polynomials
  zreal<- z[is.real(z)] # compare the imaginary roots of z close to zero
  if (is.empty(zreal)) # check if real roots exist
      { return(NA) }
  zpos<- zreal[Re(zreal) > 0] # look for positive real roots
  if (is.empty(zpos))
      { return(NA) ; }
  return(min(Re(zpos))) # return the minimal one
}





